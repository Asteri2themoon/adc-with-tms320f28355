/*
 * config.h
 *
 *  Created on: 6 d�c. 2018
 *      Author: aklipfel
 */

#ifndef CONFIG_H_
#define CONFIG_H_

//******************************** system protection ********************************

#define  EALLOW asm(" EALLOW")
#define  EDIS   asm(" EDIS")
#define  ESTOP0 asm(" ESTOP0")
#define  NOP    asm(" NOP")

//******************************** system clock ********************************

#define PLLSTS     (*((struct PLLSTS_struct*)0x7011))
#define HISPCP     (*((struct HISPCP_struct*)0x701A))
#define LOSPCP     (*((struct LOSPCP_struct*)0x701B))
#define PCLKCR0    (*((struct PCLKCR0_struct*)0x701C))
#define PCLKCR1    (*((struct PCLKCR1_struct*)0x701D))
#define PCLKCR3    (*((struct PCLKCR3_struct*)0x7020))
#define PLLCR      (*((struct PLLCR_struct*)0x7021))

struct PLLSTS_struct{
    unsigned int PLLLOCKS:1;
    unsigned int :1;
    unsigned int PLLOFF:1;
    unsigned int MCLKSTS:1;
    unsigned int MCLKCLR:1;
    unsigned int OSCOFF:1;
    unsigned int MCLKOFF:1;
    unsigned int DIVSEL:2;
    unsigned int :7;
};

struct HISPCP_struct{
    unsigned int HSPCLK:3;
    unsigned int :13;
};

struct LOSPCP_struct{
    unsigned int LSPCLK:3;
    unsigned int :13;
};

struct PCLKCR0_struct{
    unsigned int :2;
    unsigned int TBCLKSYNC:1;
    unsigned int ADCENCLK:1;
    unsigned int I2CENCLK:1;
    unsigned int SCICENCLK:1;
    unsigned int :2;
    unsigned int SPIAENCLK:1;
    unsigned int :1;
    unsigned int SCIAENCLK:1;
    unsigned int SCIBENCLK:1;
    unsigned int MCBSPAENCLK:1;
    unsigned int MCBSPBENCLK:1;
    unsigned int ECANAENCLK:1;
    unsigned int ECANBENCLK:1;
};

struct PCLKCR1_struct{
    unsigned int EPWM1ENCLK:1;
    unsigned int EPWM2ENCLK:1;
    unsigned int EPWM3ENCLK:1;
    unsigned int EPWM4ENCLK:1;
    unsigned int EPWM5ENCLK:1;
    unsigned int EPWM6ENCLK:1;
    unsigned int :2;
    unsigned int ECAP1ENCLK:1;
    unsigned int ECAP2ENCLK:1;
    unsigned int ECAP3ENCLK:1;
    unsigned int ECAP4ENCLK:1;
    unsigned int ECAP5ENCLK:1;
    unsigned int ECAP6ENCLK:1;
    unsigned int EQEP1ENCLK:1;
    unsigned int EQEP2ENCLK:1;
};

struct PCLKCR3_struct{
    unsigned int :8;
    unsigned int CPUTIMER0ENCLK:1;
    unsigned int CPUTIMER1ENCLK:1;
    unsigned int CPUTIMER2ENCLK:1;
    unsigned int DMAENCLK:1;
    unsigned int XINTFENCLK:1;
    unsigned int GPIOINENCLK:1;
    unsigned int :2;
};

struct PLLCR_struct{
    unsigned int DIV:4;
    unsigned int :12;
};

//******************************** watchdog ********************************

#define SCSR   (*((struct SCSR_struct*)0x7022))
#define WDCNTR (*((unsigned char*)0x7023))
#define WDKEY  (*((unsigned char*)0x7025))
#define WDCR  (*((unsigned int*)0x7029))

struct SCSR_struct{
    unsigned int WDOVERRIDE:1;
    unsigned int WDENINT:1;
    unsigned int WDINTS:1;
    unsigned int :13;
};

struct WDCR_struct{
    unsigned int WDPS:3;
    unsigned int WDCHK:3;
    unsigned int WDDIS:1;
    unsigned int WDFLAG:1;
    unsigned int :8;
};

//******************************** timer ********************************

#define TIMER0TIM  (*((unsigned long*)0x0C00))
#define TIMER0PRD  (*((unsigned long*)0x0C02))
#define TIMER0TCR  (*((struct TIMERxTCR_struct*)0x0C04))
#define TIMER0TPR  (*((struct TIMERxTPR_struct*)0x0C06))

#define TIMER1TIM  (*((unsigned long*)0x0C08))
#define TIMER1PRD  (*((unsigned long*)0x0C0A))
#define TIMER1TCR  (*((struct TIMERxTCR_struct*)0x0C0C))
#define TIMER1TPR  (*((struct TIMERxTPR_struct*)0x0C0E))

#define TIMER2TIM  (*((unsigned long*)0x0C10))
#define TIMER2PRD  (*((unsigned long*)0x0C12))
#define TIMER2TCR  (*((struct TIMERxTCR_struct*)0x0C14))
#define TIMER2TPR  (*((struct TIMERxTPR_struct*)0x0C16))

struct TIMERxTCR_struct{
    unsigned int :4;
    unsigned int TSS:1;
    unsigned int TRB:1;
    unsigned int :4;
    unsigned int SOFT:1;
    unsigned int FREE:1;
    unsigned int :2;
    unsigned int TIE:1;
    unsigned int TIF:1;
};

struct TIMERxTPR_struct{
    unsigned int TDDR:8;
    unsigned int PSC:8;
    unsigned int TDDRH:8;
    unsigned int PSCH:8;
};

//******************************** GPIO ********************************

#define GPACTRL     (*((struct GPxCTRL_struct*)0x6F80))
#define GPAMUX1     (*((struct GPIO_0_15_struct*)0x6F86))
#define GPAMUX2     (*((struct GPIO_16_31_struct*)0x6F88))
#define GPADIR      (*((struct GPIO_0_31_struct*)0x6F8A))
#define GPAPUD      (*((struct GPIO_0_31_struct*)0x6F8C))
#define GPBCTRL     (*((struct GPxCTRL_struct*)0x6F90))
#define GPBMUX1     (*((struct GPIO_32_47_struct*)0x6F96))
#define GPBMUX2     (*((struct GPIO_48_63_struct*)0x6F98))
#define GPBDIR      (*((struct GPIO_32_63_struct*)0x6F9A))
#define GPBPUD      (*((struct GPIO_32_63_struct*)0x6F9C))
#define GPCMUX1     (*((struct GPIO_64_79_struct*)0x6FA6))
#define GPCMUX2     (*((struct GPIO_80_87_struct*)0x6FA8))
#define GPCDIR      (*((struct GPIO_64_87_struct*)0x6FAA))
#define GPCPUD      (*((struct GPIO_64_87_struct*)0x6FAC))

#define GPADAT      (*((struct GPIO_0_31_struct*)0x6FC0))
#define GPASET      (*((struct GPIO_0_31_struct*)0x6FC2))
#define GPACLEAR    (*((struct GPIO_0_31_struct*)0x6FC4))
#define GPATOGGLE   (*((struct GPIO_0_31_struct*)0x6FC6))

#define GPBDAT      (*((struct GPIO_32_63_struct*)0x6FC8))
#define GPBSET      (*((struct GPIO_32_63_struct*)0x6FCA))
#define GPBCLEAR    (*((struct GPIO_32_63_struct*)0x6FCC))
#define GPBTOGGLE   (*((struct GPIO_32_63_struct*)0x6FCE))

#define GPCDAT      (*((struct GPIO_64_87_struct*)0x6FD0))
#define GPCSET      (*((struct GPIO_64_87_struct*)0x6FD2))
#define GPCCLEAR    (*((struct GPIO_64_87_struct*)0x6FD4))
#define GPCTOGGLE   (*((struct GPIO_64_87_struct*)0x6FD6))

struct GPIO_0_15_struct{
    unsigned int GPIO0:2;
    unsigned int GPIO1:2;
    unsigned int GPIO2:2;
    unsigned int GPIO3:2;
    unsigned int GPIO4:2;
    unsigned int GPIO5:2;
    unsigned int GPIO6:2;
    unsigned int GPIO7:2;
    unsigned int GPIO8:2;
    unsigned int GPIO9:2;
    unsigned int GPIO10:2;
    unsigned int GPIO11:2;
    unsigned int GPIO12:2;
    unsigned int GPIO13:2;
    unsigned int GPIO14:2;
    unsigned int GPIO15:2;
};

struct GPIO_16_31_struct{
    unsigned int GPIO16:2;
    unsigned int GPIO17:2;
    unsigned int GPIO18:2;
    unsigned int GPIO19:2;
    unsigned int GPIO20:2;
    unsigned int GPIO21:2;
    unsigned int GPIO22:2;
    unsigned int GPIO23:2;
    unsigned int GPIO24:2;
    unsigned int GPIO25:2;
    unsigned int GPIO26:2;
    unsigned int GPIO27:2;
    unsigned int GPIO28:2;
    unsigned int GPIO29:2;
    unsigned int GPIO30:2;
    unsigned int GPIO31:2;
};

struct GPIO_0_31_struct{
    unsigned int GPIO0:1;
    unsigned int GPIO1:1;
    unsigned int GPIO2:1;
    unsigned int GPIO3:1;
    unsigned int GPIO4:1;
    unsigned int GPIO5:1;
    unsigned int GPIO6:1;
    unsigned int GPIO7:1;
    unsigned int GPIO8:1;
    unsigned int GPIO9:1;
    unsigned int GPIO10:1;
    unsigned int GPIO11:1;
    unsigned int GPIO12:1;
    unsigned int GPIO13:1;
    unsigned int GPIO14:1;
    unsigned int GPIO15:1;
    unsigned int GPIO16:1;
    unsigned int GPIO17:1;
    unsigned int GPIO18:1;
    unsigned int GPIO19:1;
    unsigned int GPIO20:1;
    unsigned int GPIO21:1;
    unsigned int GPIO22:1;
    unsigned int GPIO23:1;
    unsigned int GPIO24:1;
    unsigned int GPIO25:1;
    unsigned int GPIO26:1;
    unsigned int GPIO27:1;
    unsigned int GPIO28:1;
    unsigned int GPIO29:1;
    unsigned int GPIO30:1;
    unsigned int GPIO31:1;
};

struct GPIO_32_47_struct{
    unsigned int GPIO32:2;
    unsigned int GPIO33:2;
    unsigned int GPIO34:2;
    unsigned int GPIO35:2;
    unsigned int GPIO36:2;
    unsigned int GPIO37:2;
    unsigned int GPIO38:2;
    unsigned int GPIO39:2;
    unsigned int GPIO40:2;
    unsigned int GPIO41:2;
    unsigned int GPIO42:2;
    unsigned int GPIO43:2;
    unsigned int GPIO44:2;
    unsigned int GPIO45:2;
    unsigned int GPIO46:2;
    unsigned int GPIO47:2;
};

struct GPIO_48_63_struct{
    unsigned int GPIO48:2;
    unsigned int GPIO49:2;
    unsigned int GPIO50:2;
    unsigned int GPIO51:2;
    unsigned int GPIO52:2;
    unsigned int GPIO53:2;
    unsigned int GPIO54:2;
    unsigned int GPIO55:2;
    unsigned int GPIO56:2;
    unsigned int GPIO57:2;
    unsigned int GPIO58:2;
    unsigned int GPIO59:2;
    unsigned int GPIO60:2;
    unsigned int GPIO61:2;
    unsigned int GPIO62:2;
    unsigned int GPIO63:2;
};

struct GPIO_32_63_struct{
    unsigned int GPIO32:1;
    unsigned int GPIO33:1;
    unsigned int GPIO34:1;
    unsigned int GPIO35:1;
    unsigned int GPIO36:1;
    unsigned int GPIO37:1;
    unsigned int GPIO38:1;
    unsigned int GPIO39:1;
    unsigned int GPIO40:1;
    unsigned int GPIO41:1;
    unsigned int GPIO42:1;
    unsigned int GPIO43:1;
    unsigned int GPIO44:1;
    unsigned int GPIO45:1;
    unsigned int GPIO46:1;
    unsigned int GPIO47:1;
    unsigned int GPIO48:1;
    unsigned int GPIO49:1;
    unsigned int GPIO50:1;
    unsigned int GPIO51:1;
    unsigned int GPIO52:1;
    unsigned int GPIO53:1;
    unsigned int GPIO54:1;
    unsigned int GPIO55:1;
    unsigned int GPIO56:1;
    unsigned int GPIO57:1;
    unsigned int GPIO58:1;
    unsigned int GPIO59:1;
    unsigned int GPIO60:1;
    unsigned int GPIO61:1;
    unsigned int GPIO62:1;
    unsigned int GPIO63:1;
};

struct GPIO_64_79_struct{
    unsigned int GPIO64:2;
    unsigned int GPIO65:2;
    unsigned int GPIO66:2;
    unsigned int GPIO67:2;
    unsigned int GPIO68:2;
    unsigned int GPIO69:2;
    unsigned int GPIO70:2;
    unsigned int GPIO71:2;
    unsigned int GPIO72:2;
    unsigned int GPIO73:2;
    unsigned int GPIO74:2;
    unsigned int GPIO75:2;
    unsigned int GPIO76:2;
    unsigned int GPIO77:2;
    unsigned int GPIO78:2;
    unsigned int GPIO79:2;
};

struct GPIO_80_87_struct{
    unsigned int GPIO80:2;
    unsigned int GPIO81:2;
    unsigned int GPIO82:2;
    unsigned int GPIO83:2;
    unsigned int GPIO84:2;
    unsigned int GPIO85:2;
    unsigned int GPIO86:2;
    unsigned int GPIO87:2;
    unsigned int :16;
};

struct GPIO_64_87_struct{
    unsigned int GPIO64:1;
    unsigned int GPIO65:1;
    unsigned int GPIO66:1;
    unsigned int GPIO67:1;
    unsigned int GPIO68:1;
    unsigned int GPIO69:1;
    unsigned int GPIO70:1;
    unsigned int GPIO71:1;
    unsigned int GPIO72:1;
    unsigned int GPIO73:1;
    unsigned int GPIO74:1;
    unsigned int GPIO75:1;
    unsigned int GPIO76:1;
    unsigned int GPIO77:1;
    unsigned int GPIO78:1;
    unsigned int GPIO79:1;
    unsigned int GPIO80:1;
    unsigned int GPIO81:1;
    unsigned int GPIO82:1;
    unsigned int GPIO83:1;
    unsigned int GPIO84:1;
    unsigned int GPIO85:1;
    unsigned int GPIO86:1;
    unsigned int GPIO87:1;
    unsigned int :8;
};

struct GPxCTRL_struct{
    unsigned long QUALPRD0:8;
    unsigned long QUALPRD1:8;
    unsigned long QUALPRD2:8;
    unsigned long QUALPRD3:8;
};

//******************************** CAP ********************************

#define ECAP1      (*((struct CAPx_struct*)0x6A00))
#define ECAP2      (*((struct CAPx_struct*)0x6A20))
#define ECAP3      (*((struct CAPx_struct*)0x6A40))
#define ECAP4      (*((struct CAPx_struct*)0x6A60))
#define ECAP5      (*((struct CAPx_struct*)0x6A80))
#define ECAP6      (*((struct CAPx_struct*)0x6AA0))

struct ECCTL1_struct{
    unsigned int CAP1POL:1;
    unsigned int CTRRST1:1;
    unsigned int CAP2POL:1;
    unsigned int CTRRST2:1;
    unsigned int CAP3POL:1;
    unsigned int CTRRST3:1;
    unsigned int CAP4POL:1;
    unsigned int CTRRST4:1;
    unsigned int CAPLDEN:1;
    unsigned int PRESCALE:5;
    unsigned int FREE_SOFT:2;
};

struct ECCTL2_struct{
    unsigned int CONT_ONESH:1;
    unsigned int STOP_WRAP:2;
    unsigned int REARM:1;
    unsigned int TSCTRSTOP:1;
    unsigned int SYNCI_EN:1;
    unsigned int SYNCO_SEL:2;
    unsigned int SWSYNC:1;
    unsigned int CAP_APWM:1;
    unsigned int APWMPOL:1;
    unsigned int :5;
};

struct ECEINT_ECFRC_struct{
    unsigned int :1;
    unsigned int CEVT1:1;
    unsigned int CEVT2:1;
    unsigned int CEVT3:1;
    unsigned int CEVT4:1;
    unsigned int CEVTOVF:1;
    unsigned int CTR_PRD:1;
    unsigned int CTR_CMP:1;
    unsigned int :8;
};

struct ECFLG_ECCLR_struct{
    unsigned int INT:1;
    unsigned int CEVT1:1;
    unsigned int CEVT2:1;
    unsigned int CEVT3:1;
    unsigned int CEVT4:1;
    unsigned int CEVTOVF:1;
    unsigned int CTR_PRD:1;
    unsigned int CTR_CMP:1;
    unsigned int :8;
};

struct CAPx_struct{
    unsigned long TSCTR;
    unsigned long CTRPHS;
    unsigned long CAP1;
    unsigned long CAP2;
    unsigned long CAP3;
    unsigned long CAP4;
    unsigned long :32;
    unsigned long :32;
    unsigned long :32;
    unsigned long :32;
    struct ECCTL1_struct ECCTL1;
    struct ECCTL2_struct ECCTL2;
    struct ECEINT_ECFRC_struct ECEINT;
    struct ECFLG_ECCLR_struct ECEFLG;
    struct ECFLG_ECCLR_struct ECCLR;
    struct ECEINT_ECFRC_struct ECEFRC;
    unsigned long :32;
    unsigned long :32;
    unsigned long :32;
};

//******************************** PIE ********************************

struct PIECTRL_struct{
    unsigned int ENPIE:1;
    unsigned int PIEVECT:15;
};
/*
struct PIEACK_struct{
    unsigned int PIEACK:12;
    unsigned int :4;
};*/

struct PIEIFRx_PIEIERx_struct{
    unsigned int INTx1:1;
    unsigned int INTx2:1;
    unsigned int INTx3:1;
    unsigned int INTx4:1;
    unsigned int INTx5:1;
    unsigned int INTx6:1;
    unsigned int INTx7:1;
    unsigned int INTx8:1;
    unsigned int :8;
};

extern cregister volatile unsigned int IFR;
extern cregister volatile unsigned int IER;
#define  EINT   asm(" clrc INTM")
#define  DINT   asm(" setc INTM")
#define  ERTM   asm(" clrc DBGM")
#define  DRTM   asm(" setc DBGM")
//extern cregister volatile unsigned int DBGIER;

struct IFR_IER_DBGIER_struct{
    unsigned int INT1:1;
    unsigned int INT2:1;
    unsigned int INT3:1;
    unsigned int INT4:1;
    unsigned int INT5:1;
    unsigned int INT6:1;
    unsigned int INT7:1;
    unsigned int INT8:1;
    unsigned int INT9:1;
    unsigned int INT10:1;
    unsigned int INT11:1;
    unsigned int INT12:1;
    unsigned int INT13:1;
    unsigned int INT14:1;
    unsigned int DLOGINT:1;
    unsigned int RTOSINT:1;
};

struct INT1_struct{
    void (*SEQ1INT)(void);
    void (*SEQ2INT)(void);
    unsigned long :32;
    void (*XINT1)(void);
    void (*XINT2)(void);
    void (*ADCINT)(void);
    void (*TINT0)(void);
    void (*WAKEINT)(void);
};

struct INT2_struct{
    void (*EPWM1_TZINT)(void);
    void (*EPWM2_TZINT)(void);
    void (*EPWM3_TZINT)(void);
    void (*EPWM4_TZINT)(void);
    void (*EPWM5_TZINT)(void);
    void (*EPWM6_TZINT)(void);
    unsigned long :32;
    unsigned long :32;
};

struct INT3_struct{
    void (*EPWM1_INT)(void);
    void (*EPWM2_INT)(void);
    void (*EPWM3_INT)(void);
    void (*EPWM4_INT)(void);
    void (*EPWM5_INT)(void);
    void (*EPWM6_INT)(void);
    unsigned long :32;
    unsigned long :32;
};

struct INT4_struct{
    void (*ECAP1_INT)(void);
    void (*ECAP2_INT)(void);
    void (*ECAP3_INT)(void);
    void (*ECAP4_INT)(void);
    void (*ECAP5_INT)(void);
    void (*ECAP6_INT)(void);
    unsigned long :32;
    unsigned long :32;
};

struct INT5_struct{
    void (*EQEP1_INT)(void);
    void (*EQEP2_INT)(void);
    unsigned long :32;
    unsigned long :32;
    unsigned long :32;
    unsigned long :32;
    unsigned long :32;
    unsigned long :32;
};

struct INT6_struct{
    void (*SPIRXINTA)(void);
    void (*SPITXINTA)(void);
    void (*MRINTB)(void);
    void (*MXINTB)(void);
    void (*MRINTA)(void);
    void (*MXINTA)(void);
    unsigned long :32;
    unsigned long :32;
};

struct INT7_struct{
    void (*DINTCH1)(void);
    void (*DINTCH2)(void);
    void (*DINTCH3)(void);
    void (*DINTCH4)(void);
    void (*DINTCH5)(void);
    void (*DINTCH6)(void);
    unsigned long :32;
    unsigned long :32;
};

struct INT8_struct{
    void (*I2CINT1A)(void);
    void (*I2CINT2A)(void);
    unsigned long :32;
    unsigned long :32;
    void (*SCIRXINTC)(void);
    void (*SCITXINTC)(void);
    unsigned long :32;
    unsigned long :32;
};

struct INT9_struct{
    void (*SCIRXINTA)(void);
    void (*SCITXINTA)(void);
    void (*SCIRXINTB)(void);
    void (*SCITXINTB)(void);
    void (*ECAN0INTA)(void);
    void (*ECAN1INTA)(void);
    void (*ECAN0INTB)(void);
    void (*ECAN1INTB)(void);
};

struct INT10_struct{
    unsigned long :32;
    unsigned long :32;
    unsigned long :32;
    unsigned long :32;
    unsigned long :32;
    unsigned long :32;
    unsigned long :32;
    unsigned long :32;
};

struct INT11_struct{
    unsigned long :32;
    unsigned long :32;
    unsigned long :32;
    unsigned long :32;
    unsigned long :32;
    unsigned long :32;
    unsigned long :32;
    unsigned long :32;
};

struct INT12_struct{
    void (*XINT3)(void);
    void (*XINT4)(void);
    void (*XINT5)(void);
    void (*XINT6)(void);
    void (*XINT7)(void);
    unsigned long :32;
    void (*LVF)(void);
    void (*LUF)(void);
};

#define PIECTRL     (*((struct PIECTRL_struct*)0x0CE0))
#define PIEACK      (*((unsigned int*)0x0CE1))
#define PIEIER1     (*((struct PIEIFRx_PIEIERx_struct*)0x0CE2))
#define PIEIFR1     (*((struct PIEIFRx_PIEIERx_struct*)0x0CE3))
#define PIEIER2     (*((struct PIEIFRx_PIEIERx_struct*)0x0CE4))
#define PIEIFR2     (*((struct PIEIFRx_PIEIERx_struct*)0x0CE5))
#define PIEIER3     (*((struct PIEIFRx_PIEIERx_struct*)0x0CE6))
#define PIEIFR3     (*((struct PIEIFRx_PIEIERx_struct*)0x0CE7))
#define PIEIER4     (*((struct PIEIFRx_PIEIERx_struct*)0x0CE8))
#define PIEIFR4     (*((struct PIEIFRx_PIEIERx_struct*)0x0CE9))
#define PIEIER5     (*((struct PIEIFRx_PIEIERx_struct*)0x0CEA))
#define PIEIFR5     (*((struct PIEIFRx_PIEIERx_struct*)0x0CEB))
#define PIEIER6     (*((struct PIEIFRx_PIEIERx_struct*)0x0CEC))
#define PIEIFR6     (*((struct PIEIFRx_PIEIERx_struct*)0x0CED))
#define PIEIER7     (*((struct PIEIFRx_PIEIERx_struct*)0x0CEE))
#define PIEIFR7     (*((struct PIEIFRx_PIEIERx_struct*)0x0CEF))
#define PIEIER8     (*((struct PIEIFRx_PIEIERx_struct*)0x0CF0))
#define PIEIFR8     (*((struct PIEIFRx_PIEIERx_struct*)0x0CF1))
#define PIEIER9     (*((struct PIEIFRx_PIEIERx_struct*)0x0CF2))
#define PIEIFR9     (*((struct PIEIFRx_PIEIERx_struct*)0x0CF3))
#define PIEIER10    (*((struct PIEIFRx_PIEIERx_struct*)0x0CF4))
#define PIEIFR10    (*((struct PIEIFRx_PIEIERx_struct*)0x0CF5))
#define PIEIER11    (*((struct PIEIFRx_PIEIERx_struct*)0x0CF6))
#define PIEIFR11    (*((struct PIEIFRx_PIEIERx_struct*)0x0CF7))
#define PIEIER12    (*((struct PIEIFRx_PIEIERx_struct*)0x0CF8))
#define PIEIFR12    (*((struct PIEIFRx_PIEIERx_struct*)0x0CF9))

#define INT1        (*((struct INT1_struct*)0xD40))
#define INT2        (*((struct INT2_struct*)0xD50))
#define INT3        (*((struct INT3_struct*)0xD60))
#define INT4        (*((struct INT4_struct*)0xD70))
#define INT5        (*((struct INT5_struct*)0xD80))
#define INT6        (*((struct INT6_struct*)0xD90))
#define INT7        (*((struct INT7_struct*)0xDA0))
#define INT8        (*((struct INT8_struct*)0xDB0))
#define INT9        (*((struct INT9_struct*)0xDC0))
#define INT10       (*((struct INT10_struct*)0xDD0))
#define INT11       (*((struct INT11_struct*)0xDE0))
#define INT12       (*((struct INT12_struct*)0xDF0))

//******************************** ADC ********************************

#define ADCTRL1       (*((struct ADCTRL1_struct*)0x7100))
#define ADCTRL2       (*((struct ADCTRL2_struct*)0x7101))
#define ADCMAXCONV    (*((struct ADCMAXCONV_struct*)0x7102))
#define ADCCHSELSEQ1  (*((struct ADCCHSELSEQ1_struct*)0x7103))
#define ADCCHSELSEQ2  (*((struct ADCCHSELSEQ2_struct*)0x7104))
#define ADCCHSELSEQ3  (*((struct ADCCHSELSEQ3_struct*)0x7105))
#define ADCCHSELSEQ4  (*((struct ADCCHSELSEQ4_struct*)0x7106))
#define ADCASEQSR     (*((struct ADCASEQSR_struct*)0x7107))
#define ADCRESULT0    (*((unsigned int*)0x7108))
#define ADCRESULT1    (*((unsigned int*)0x7109))
#define ADCRESULT2    (*((unsigned int*)0x710A))
#define ADCRESULT3    (*((unsigned int*)0x710B))
#define ADCRESULT4    (*((unsigned int*)0x710C))
#define ADCRESULT5    (*((unsigned int*)0x710D))
#define ADCRESULT6    (*((unsigned int*)0x710E))
#define ADCRESULT7    (*((unsigned int*)0x710F))
#define ADCRESULT8    (*((unsigned int*)0x7110))
#define ADCRESULT9    (*((unsigned int*)0x7111))
#define ADCRESULT10   (*((unsigned int*)0x7112))
#define ADCRESULT11   (*((unsigned int*)0x7113))
#define ADCRESULT12   (*((unsigned int*)0x7114))
#define ADCRESULT13   (*((unsigned int*)0x7115))
#define ADCRESULT14   (*((unsigned int*)0x7116))
#define ADCRESULT15   (*((unsigned int*)0x7117))
#define ADCTRL3       (*((struct ADCTRL3_struct*)0x7118))
#define ADCST         (*((struct ADCST_struct*)0x7119))
#define ADCREFSEL     (*((struct ADCREFSEL_struct*)0x711C))
#define ADCOFFTRIM    (*((struct ADCOFFTRIM_struct*)0x711D))

struct ADCTRL1_struct{
    unsigned int :4;
    unsigned int SEQ_CASC:1;
    unsigned int SEQ_OVRD:1;
    unsigned int CONT_RUN:1;
    unsigned int CPS:1;
    unsigned int ACQ_PS:4;
    unsigned int SUSMOD:2;
    unsigned int RESET:1;
    unsigned int :1;
};

struct ADCTRL2_struct{
    unsigned int EPWM_SOCB_SEQ2:1;
    unsigned int :1;
    unsigned int INT_MOD_SEQ2:1;
    unsigned int INT_ENA_SEQ2:1;
    unsigned int :1;
    unsigned int SOC_SEQ2:1;
    unsigned int RST_SEQ2:1;
    unsigned int EXT_SOC_SEQ1:1;
    unsigned int EPWM_SOCA_SEQ1:1;
    unsigned int :1;
    unsigned int INT_MOD_SEQ1:1;
    unsigned int INT_ENA_SEQ1:1;
    unsigned int :1;
    unsigned int SOC_SEQ1:1;
    unsigned int RST_SEQ1:1;
    unsigned int EPWM_SOCB_SEQ:1;
};

struct ADCMAXCONV_struct{
    unsigned int MAX_CONV1:4;
    unsigned int MAX_CONV2:3;
    unsigned int :9;
};

struct ADCCHSELSEQ1_struct{
    unsigned int CONV00:4;
    unsigned int CONV01:4;
    unsigned int CONV02:4;
    unsigned int CONV03:4;
};

struct ADCCHSELSEQ2_struct{
    unsigned int CONV04:4;
    unsigned int CONV05:4;
    unsigned int CONV06:4;
    unsigned int CONV07:4;
};

struct ADCCHSELSEQ3_struct{
    unsigned int CONV08:4;
    unsigned int CONV09:4;
    unsigned int CONV10:4;
    unsigned int CONV11:4;
};

struct ADCCHSELSEQ4_struct{
    unsigned int CONV12:4;
    unsigned int CONV13:4;
    unsigned int CONV14:4;
    unsigned int CONV15:4;
};

struct ADCASEQSR_struct{
    unsigned int SEQ1_STATE:4;
    unsigned int SEQ2_STATE:3;
    unsigned int :1;
    unsigned int SEQ_CNTR:4;
    unsigned int :4;
};

struct ADCST_struct{
    unsigned int INT_SEQ1:1;
    unsigned int INT_SEQ2:1;
    unsigned int SEQ1_BSY:1;
    unsigned int SEQ2_BSY:1;
    unsigned int INT_SEQ1_CLR:1;
    unsigned int INT_SEQ2_CLR:1;
    unsigned int EOS_BUF1:1;
    unsigned int EOS_BUF2:1;
    unsigned int :8;
};

struct ADCREFSEL_struct{
    unsigned int :14;
    unsigned int REF_SEL:2;
};

struct ADCOFFTRIM_struct{
    unsigned int OFFSET_TRIM:9;
    unsigned int :7;
};

struct ADCTRL3_struct{
    unsigned int SMODE_SEL:1;
    unsigned int ADCCLKPS:4;
    unsigned int ADCPWDN:1;
    unsigned int ADCBGRFDN:2;
    unsigned int :8;
};

//******************************** DMA ********************************

#define DMACTRL         (*((struct DMACTRL_struct*)0x1000))
#define DEBUGCTRL       (*((struct DEBUGCTRL_struct*)0x1001))
#define REVISION        (*((struct REVISION_struct*)0x1002))
#define PRIORITYCTRL1   (*((struct PRIORITYCTRL1_struct*)0x1004))
#define PRIORITYSTAT    (*((struct PRIORITYSTAT_struct*)0x1006))

#define DMA_CHANNEL_1   (*((struct DMA_CHANNEL_struct*)0x1020))
#define DMA_CHANNEL_2   (*((struct DMA_CHANNEL_struct*)0x1040))
#define DMA_CHANNEL_3   (*((struct DMA_CHANNEL_struct*)0x1060))
#define DMA_CHANNEL_4   (*((struct DMA_CHANNEL_struct*)0x1080))
#define DMA_CHANNEL_5   (*((struct DMA_CHANNEL_struct*)0x10A0))
#define DMA_CHANNEL_6   (*((struct DMA_CHANNEL_struct*)0x10C0))

struct DMACTRL_struct{
    unsigned int HARD_RESET:1;
    unsigned int PRIORITY_RESET:1;
    unsigned int :14;
};

struct DEBUGCTRL_struct{
    unsigned int :15;
    unsigned int FREE:1;
};

struct RESISION_struct{
    unsigned int REV:8;
    unsigned int TYPE:8;
};

struct PRIORITYCTRL1_struct{
    unsigned int PRIORITY:1;
    unsigned int :15;
};

struct PRIORITYSTAT_struct{
    unsigned int ACTIVESTS:3;
    unsigned int :1;
    unsigned int ACTIVESTS_SHADOW:3;
    unsigned int :9;
};

struct MODE_struct{
    unsigned int PERINTSEL:5;
    unsigned int :2;
    unsigned int OVRINTE:1;
    unsigned int PERINTE:1;
    unsigned int CHINTMODE:1;
    unsigned int ONESHOT:1;
    unsigned int CONTINUOUS:1;
    unsigned int SYNCE:1;
    unsigned int SYNCSEL:1;
    unsigned int DATASIZE:1;
    unsigned int CHINTE:1;
};

struct CONTROL_struct{
    unsigned int RUN:1;
    unsigned int HATL:1;
    unsigned int SOTFRESET:1;
    unsigned int PERINTFRC:1;
    unsigned int PERINTCLR:1;
    unsigned int SYNCFRC:1;
    unsigned int SYNCCLR:1;
    unsigned int ERRCLR:1;
    unsigned int PERINTFLG:1;
    unsigned int SYNCFLG:1;
    unsigned int SYNCERR:1;
    unsigned int TRANSFERST:1;
    unsigned int BURSTSTS:1;
    unsigned int RUNSTS:1;
    unsigned int OVRFLG:1;
    unsigned int :1;
};

struct BURST_SIZE_struct{
    unsigned int BURSTSIZE:5;
    unsigned int :11;
};

struct BURST_COUNT_struct{
    unsigned int BURSTCOUNT:5;
    unsigned int :11;
};

struct BEGIN_ADDRESS_POINTER_struct{
    unsigned int *BEGADDR;
};

struct DMA_CHANNEL_struct{
    struct MODE_struct MODE;
    struct CONTROL_struct CONTROL;
    unsigned int BURST_SIZE;
    unsigned int BURST_COUNT;
    int SRC_BURST_STEP;
    int DST_BURST_STEP;
    unsigned int TRANSFER_SIZE;
    unsigned int TRANSFER_COUNT;
    int SRC_TRANSFER_STEP;
    int DST_TRANSFER_STEP;
    unsigned int SRC_WRAP_SIZE;
    unsigned int SRC_WRAP_COUNT;
    int SRC_WRAP_STEP;
    unsigned int DST_WRAP_SIZE;
    unsigned int DST_WRAP_COUNT;
    int DST_WRAP_STEP;
    void *SRC_BEG_ADDR_SHADOW;
    void *SRC_ADDR_SHADOW;
    void *SRC_BEG_ADDR;
    void *SRC_ADDR;
    void *DST_BEG_ADDR_SHADOW;
    void *DST_ADDR_SHADOW;
    void *DST_BEG_ADDR;
    void *DST_ADDR;
};

#endif /* CONFIG_H_ */
