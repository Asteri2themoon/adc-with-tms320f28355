#include "config.h"
#include "system.h"

/**
 * main.c
 */

/*#pragma DATA_SECTION(DMABuf1,"DMARAML4");
volatile unsigned int DMABuf1[0x0FF0]={4};*/
#pragma DATA_SECTION(DMABuf2,"DMARAML5");
volatile unsigned int DMABuf2[0x1000]={5};
#pragma DATA_SECTION(DMABuf3,"DMARAML6");
volatile unsigned int DMABuf3[0x1000]={6};
#pragma DATA_SECTION(DMABuf4,"DMARAML7");
volatile unsigned int DMABuf4[0x1000]={7};

volatile unsigned int input[0x20];

void initADC();
void initDMA();

void main(void)
{
    init();

    //gpio config
    EALLOW;
    GPAPUD.GPIO9=1;
    GPAMUX1.GPIO9=0;
    GPADIR.GPIO9=1;
    GPAPUD.GPIO11=1;
    GPAMUX1.GPIO11=0;
    GPADIR.GPIO11=1;
    EDIS;
    GPADAT.GPIO9=0;
    GPADAT.GPIO11=0;

    initADC();
    initDMA();

    int i;
    for(i=0;i<0x1000;i++){
        DMABuf2[i]=0;
    }

    //read
    ADCTRL2.RST_SEQ1 = 1;
    ADCTRL2.SOC_SEQ1 = 1;
    for(;;){
        delay(1);
        /*EALLOW;
        DMA_CHANNEL_1.CONTROL.PERINTCLR=1;//clear flag
        EDIS;*/
    }
}

void initADC(){
    EALLOW;
    HISPCP.HSPCLK=0x3;//25MHz at 150MHz SYSCLKOUT
    PCLKCR0.ADCENCLK=1;//enable ADC clock
    EDIS;

    ADCREFSEL.REF_SEL=0;
    ADCOFFTRIM.OFFSET_TRIM=0;

    ADCMAXCONV.MAX_CONV1= 0x00; // Setup 1 conversion on SEQ 1
    ADCCHSELSEQ1.CONV00 = 0x2; // Setup ADCINA0 as 1st SEQ conversion.
    ADCCHSELSEQ1.CONV01 = 0x1; // Setup ADCINA1 as 2st SEQ conversion.

    ADCTRL1.SEQ_CASC = 0;
    ADCTRL1.SEQ_OVRD = 0;
    ADCTRL1.CONT_RUN = 1;
    ADCTRL1.CPS = 0;//no division
    ADCTRL1.ACQ_PS = 0b0000;
    ADCTRL1.SUSMOD = 0b01;

    ADCTRL2.RST_SEQ1=1;//Immediately reset sequencer to state CONV00
    ADCTRL2.INT_ENA_SEQ1=1;//enable SEQ1 interrupt

    ADCTRL3.ADCBGRFDN=0x3;//bandgap on
    ADCTRL3.ADCPWDN=1;//analog circuitry on
    ADCTRL3.ADCCLKPS=0;//how need clock divider?
    ADCTRL3.SMODE_SEL=0;//single sampling mode
}

void initDMA(){
    EALLOW;
    PCLKCR3.DMAENCLK=1;
    DEBUGCTRL.FREE=0;
    PRIORITYCTRL1.PRIORITY=1;//high priority channel 1
    //DMA_CHANNEL_1.MODE.SYNCSEL=0;//src
    DMA_CHANNEL_1.MODE.SYNCE=0;//ADC sync
    DMA_CHANNEL_1.MODE.CONTINUOUS=0;
    DMA_CHANNEL_1.MODE.PERINTSEL=1;//from ADC
    DMA_CHANNEL_1.MODE.PERINTE=1;//enable interrupt trigger
    DMA_CHANNEL_1.CONTROL.RUN=1;//start dma channel 1
    DMA_CHANNEL_1.BURST_SIZE=0;//1 word
    DMA_CHANNEL_1.SRC_BURST_STEP=0;
    DMA_CHANNEL_1.DST_BURST_STEP=0;
    DMA_CHANNEL_1.TRANSFER_SIZE=0x0FFF;//16 burst
    DMA_CHANNEL_1.SRC_TRANSFER_STEP=0;
    DMA_CHANNEL_1.DST_TRANSFER_STEP=1;
    DMA_CHANNEL_1.SRC_WRAP_SIZE=0x1000;
    DMA_CHANNEL_1.SRC_WRAP_STEP=0;
    DMA_CHANNEL_1.DST_WRAP_SIZE=0x1000;//4 transfer
    DMA_CHANNEL_1.DST_WRAP_STEP=0;
    DMA_CHANNEL_1.SRC_ADDR_SHADOW=(void*)0x0B00;
    DMA_CHANNEL_1.DST_ADDR_SHADOW=(void*)&DMABuf2[0];
    DMA_CHANNEL_1.SRC_BEG_ADDR_SHADOW=(void*)0x0B00;
    DMA_CHANNEL_1.DST_BEG_ADDR_SHADOW=(void*)&DMABuf2[0];
    EDIS;
}
