/*
 *  ======= system ========
 *  system target-side implementation
 *
 *  Created on: 17 d�c. 2018
 *  Author:     aklipfel
 */
/*#include <xdc/std.h>
#include <xdc/runtime/Startup.h>
*/
#include "system.h"
#include "config.h"

void init(){
    __eallow();
    WDCR|=0x68;//disable watchdog
    __edis();

    __eallow();
    //configure device clock to 150 MHz (with 30MHz oscillator)
    if(PLLSTS.MCLKSTS){
        //Error
    }else{
        if(PLLSTS.DIVSEL>=2){//Should operate in /4 division mode
            PLLSTS.DIVSEL=0;
        }
        PLLSTS.MCLKOFF=1;//Disable failed oscillator detect logic
        PLLCR.DIV=10;//PLL multiplier to 10
        while(!PLLSTS.PLLLOCKS);//Waiting for PLL to lock
        PLLSTS.MCLKOFF=0;//Enable failed oscillator detect logic
        PLLSTS.DIVSEL=2;//back to /2 division mode
    }
    __edis();

    //timer 0 config
    TIMER0PRD=0xFFFFFFFF;
    TIMER0TCR.TIE=0;//disable interrupt
    TIMER0TCR.TSS=0;//restart timer 0
}

void delay(unsigned long dt){//delay in ms
    TIMER0TCR.TSS=0;//restart timer 0
    TIMER0TIM=0;//reset timer 0
    unsigned long timeout=TIMER0PRD-(getSystemFrequency()/1000*dt);//calculate timeout value
    while(TIMER0TIM>timeout);
}

unsigned long getSystemFrequency(){
    static int div[]={4,4,2,1};
    return (CRYSTAL_FREQUENCY*PLLCR.DIV/div[PLLSTS.DIVSEL]);
}
